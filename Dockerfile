FROM python:3.9.14-slim-bullseye

WORKDIR /app

COPY . /app

RUN pip install -r requirements.txt && rm requirements.txt
RUN mkdir db && sh init.sh && rm init.sh && chown -R 1000:1000 /app

USER 1000
EXPOSE 5000

ENTRYPOINT ["python"]
CMD ["manage.py", "runserver", "0.0.0.0:5000"]
